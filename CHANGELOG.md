# Changelog

## [Unreleased]

## [0.7.0] - 2024-01-06

* Drop support for python 3.8
* Drop use of wagail.conrib.modeladmin
* Update dependencies

## [0.6.0] - 2023-05-10

* Add support for wagtail 5.x
* Add migration to be compatible with default django migraitons mechanism when django-sb-simple-migrations is not used

## [0.5.0] - 2023-04-29

* Update package description in pyproject.toml

## [0.4.0] - 2023-04-15

* Remove codecov from development requirements
* Add wagtail_sb_fontawesome to requirements
* Update repository references to gitlab

## [0.3.0] - 2023-02-22

* Remove innecesary template and statci files
* Update README.md
* Add reference to django-admin-interface as inspiration, thanks to @fabiocaccamo :smile:

## [0.1.0] - 2023-02-22

* Fix README.md
* Remove reference to requires.io.

## [0.0.0] - 2023-02-22

* Add support for change Wagtail admin colors
* Add support for change wagtail admin logo
